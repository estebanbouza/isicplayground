class AddStatusToS3FileDigestor < ActiveRecord::Migration[5.1]
  def change
    add_column :s3_file_digestors, :status, :string
  end
end
