class AddReportToProfiles < ActiveRecord::Migration[5.1]
  def up
    add_reference :profiles, :report, foreign_key: true, default: nil

    Profile.transaction do
      report = Report.first
      Profile.update_all report_id: report.id
    end
  end

  def down
    remove_column :profiles, :report_id
  end
end
