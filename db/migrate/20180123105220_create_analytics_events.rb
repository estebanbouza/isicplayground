class CreateAnalyticsEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :analytics_events do |t|

      t.timestamps only: :updated_at
    end
  end
end
