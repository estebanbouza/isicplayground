class AddAttributesToAnalyticsEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :analytics_events, :datetime, :datetime
    add_column :analytics_events, :user_id, :bigint
    add_column :analytics_events, :latitude, :float
    add_column :analytics_events, :longitude, :float
    add_column :analytics_events, :location, :string
    add_column :analytics_events, :user_agent, :string
    add_column :analytics_events, :ip, :string
    add_column :analytics_events, :country_id, :string
    add_column :analytics_events, :institute_id, :string
    add_column :analytics_events, :language, :string
    add_column :analytics_events, :app_version, :string
    add_column :analytics_events, :card_id, :string
    add_column :analytics_events, :ccdb_card_id, :string
    add_column :analytics_events, :session_id, :string
    add_column :analytics_events, :action_type_code, :string
    add_column :analytics_events, :action_params, :string
    add_column :analytics_events, :fired, :bool, default: false
    add_column :analytics_events, :uuid, :uuid

    add_index :analytics_events, :datetime
    add_index :analytics_events, :fired

  end
end
