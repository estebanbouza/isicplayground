class AddUniqueIndexToReportVersion < ActiveRecord::Migration[5.1]
  def change
    remove_index :reports, :version
    add_index :reports, :version, unique: true
  end
end
