class UpdateSiblingForeignKey < ActiveRecord::Migration[5.1]
  def change
    remove_reference :profiles, :profile
    add_reference :profiles, :profile

    add_foreign_key :profiles, :profiles, on_delete: :cascade
  end
end
