class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.timestamp :date
      t.integer :version
      t.string :name
      t.integer :count

      t.timestamps
    end

    add_index :reports, :version

    Report.create(
              version: 1,
              name: 'First report'
    )

  end
end
