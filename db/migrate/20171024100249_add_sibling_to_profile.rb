class AddSiblingToProfile < ActiveRecord::Migration[5.1]
  def change
    add_reference :profiles, :profile, foreign_key: true, default: nil
  end
end
