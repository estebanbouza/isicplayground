class ChangeUserIdTypeInAnalyticsEvents < ActiveRecord::Migration[5.1]
  def self.up
     change_column :analytics_events, :user_id, :string
  end
end
