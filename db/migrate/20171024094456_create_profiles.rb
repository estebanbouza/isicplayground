class CreateProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :profiles do |t|
      t.integer :platform_id
      t.string :card_holder_name
      t.string :email
      t.string :photo
      t.boolean :photo_upload_allowed
      t.string :card_number
      t.timestamp :valid_from
      t.timestamp :valid_to
      t.boolean :is_profile_present_on_ccdb
      t.string :platform_name

      t.timestamps
    end
    add_index :profiles, :platform_id
    add_index :profiles, :card_holder_name
    add_index :profiles, :email
    add_index :profiles, :photo
    add_index :profiles, :photo_upload_allowed
    add_index :profiles, :card_number
    add_index :profiles, :valid_from
    add_index :profiles, :valid_to
    add_index :profiles, :is_profile_present_on_ccdb
    add_index :profiles, :platform_name
  end
end
