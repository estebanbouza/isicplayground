class CreateS3FileDigestors < ActiveRecord::Migration[5.1]
  def change
    create_table :s3_file_digestors do |t|
      t.string :name
      t.datetime :belladati_processed_at, default: nil

      t.timestamps
    end
    add_index :s3_file_digestors, :name
    add_index :s3_file_digestors, :belladati_processed_at
  end
end
