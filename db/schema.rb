# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180130102947) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "analytics_events", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "datetime"
    t.string "user_id"
    t.float "latitude"
    t.float "longitude"
    t.string "location"
    t.string "user_agent"
    t.string "ip"
    t.string "country_id"
    t.string "institute_id"
    t.string "language"
    t.string "app_version"
    t.string "card_id"
    t.string "ccdb_card_id"
    t.string "session_id"
    t.string "action_type_code"
    t.string "action_params"
    t.boolean "fired", default: false
    t.uuid "uuid"
    t.index ["datetime"], name: "index_analytics_events_on_datetime"
    t.index ["fired"], name: "index_analytics_events_on_fired"
  end

  create_table "profiles", force: :cascade do |t|
    t.integer "platform_id"
    t.string "card_holder_name"
    t.string "email"
    t.string "photo"
    t.boolean "photo_upload_allowed"
    t.string "card_number"
    t.datetime "valid_from"
    t.datetime "valid_to"
    t.boolean "is_profile_present_on_ccdb"
    t.string "platform_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "report_id"
    t.bigint "profile_id"
    t.index ["card_holder_name"], name: "index_profiles_on_card_holder_name"
    t.index ["card_number"], name: "index_profiles_on_card_number"
    t.index ["email"], name: "index_profiles_on_email"
    t.index ["is_profile_present_on_ccdb"], name: "index_profiles_on_is_profile_present_on_ccdb"
    t.index ["photo"], name: "index_profiles_on_photo"
    t.index ["photo_upload_allowed"], name: "index_profiles_on_photo_upload_allowed"
    t.index ["platform_id"], name: "index_profiles_on_platform_id"
    t.index ["platform_name"], name: "index_profiles_on_platform_name"
    t.index ["profile_id"], name: "index_profiles_on_profile_id"
    t.index ["report_id"], name: "index_profiles_on_report_id"
    t.index ["valid_from"], name: "index_profiles_on_valid_from"
    t.index ["valid_to"], name: "index_profiles_on_valid_to"
  end

  create_table "reports", force: :cascade do |t|
    t.datetime "date"
    t.integer "version"
    t.string "name"
    t.integer "count"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["version"], name: "index_reports_on_version", unique: true
  end

  create_table "s3_file_digestors", force: :cascade do |t|
    t.string "name"
    t.datetime "belladati_processed_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "status"
    t.index ["belladati_processed_at"], name: "index_s3_file_digestors_on_belladati_processed_at"
    t.index ["name"], name: "index_s3_file_digestors_on_name"
  end

  add_foreign_key "profiles", "profiles", on_delete: :cascade
  add_foreign_key "profiles", "reports"
end
