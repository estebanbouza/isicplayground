class DigestRecentS3AnalyticsJob < ApplicationJob
  queue_as :default

  def perform(*args)
    S3FileDigestor.digest_s3_to_belladati count: 1
  end
end
