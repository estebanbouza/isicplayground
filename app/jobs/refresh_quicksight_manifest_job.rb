class RefreshQuicksightManifestJob < ApplicationJob
  queue_as :default

  def perform(*args)
    S3FileDigestor.generate_s3_manifest
  end
end
