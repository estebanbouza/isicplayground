class Report < ApplicationRecord
  has_many :profiles, dependent: :destroy

  def diff_report(limit = 0)
    differences_count = profiles.count/2
    logger.info "There are #{differences_count} different profiles"

    orphans = profiles.where(profile_id: nil)
    logger.info "#{orphans.count} do not have a matching profile in the other platform"

    fields = different_fields(limit)
    logger.info "Fields differing: #{fields.inspect}"
    logger.info ap(fields)

    character_distribution = character_distribution()

    {
        fields: fields,
        differences_count: differences_count,
        orphans: orphans.count,
        character_distribution: character_distribution
    }
  end


  def different_fields(limit)
    the_limit = limit.zero? ? 10_000_000 : limit
    siblings = profiles.where(platform_name: 'mw')
                   .includes(:sibling)
                   .order(created_at: :asc)
                   .limit(the_limit)
                   .map do |profile|
      profile.sibling_difference.map {|key, _| {key => 1}}.reduce(:merge)
    end

    reduced = siblings.reduce do |x, y|
      # logger.debug "\nReducing #{x.inspect} and #{y.inspect}"
      first = x || {}
      second = y || {}

      second.merge(first) {|key, old, new| new + old}
    end

    sorted = reduced.sort {|x, y| y[1] <=> x[1]}

  end

  def character_distribution
    [:mw, :ccdb].map do |platform|
      {
          platform => profiles.where(platform_name: platform)
                          .pluck(:card_number)
                          .map {|c| c.length}
                          .reduce do |a, b|
            item = a.is_a?(Hash) ? a : {a => 1}
            item.merge(item.dig(b).nil? ? {b => 1} : {b => item[b] + 1})
          end
      }
    end.reduce(&:merge)

  end

end
