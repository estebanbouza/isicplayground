class S3FileDigestor < ApplicationRecord
  class << self
    def digest_s3_to_belladati(opts = {})
      count = opts[:count] || 1

      objects = s3_client.list_objects(bucket: s3_bucket)

      keys = objects.contents.map &:key
      keys.select! { |x| x.include? 'json/' }.sort! { |a, b| a <=> b }

      processed_keys = S3FileDigestor
                       .where.not(belladati_processed_at: nil)
                       .or(S3FileDigestor
                          .where(status: :processing))
                       .pluck :name

      keys -= processed_keys

      logger.info 'No new file to process. Skipping...' if keys.blank?

      keys.first(count).each { |k| digest_s3_object_to_belladati(k) }
    end

    def digest_s3_object_to_belladati(obj_key = '')
      LockManager.lock!("digest-lock-#{obj_key}", 60_000) do
        fd = S3FileDigestor.find_or_create_by name: obj_key
        fd.update! status: :processing

        logger.info "Digesting #{obj_key} to belladati..."
        logger.info 'Retrieving contents from S3...'
        obj_contents = s3_client.get_object(bucket: s3_bucket, key: obj_key).body.string
        events = obj_contents.split /$/

        analytics_events = []
        events.each do |ev_s|
          next unless ev_s.present?
          ev = JSON.parse ev_s
          analytics_events << AnalyticsEvent.new(ev)
        end
        logger.info "Found #{analytics_events.count} events to digest. Importing to Belladati..."
        AnalyticsEvent.fire_list! analytics_events

        fd.update! belladati_processed_at: Time.current, status: :completed
      end
    end

    def generate_s3_manifest(opts = {})
      base = opts[:base] || 'https://s3.amazonaws.com/dmi-bi-logs-test/'
      file = opts[:file] || "#{ENV.fetch('HOME')}/Downloads/quicksight-manifest-#{Time.current.to_i}.json"
      put = opts[:put] || false

      logger.info 'Retrieving bucket objects...'
      l = S3Client.list_objects bucket: 'dmi-bi-logs-test'

      # Select those files with content
      urls = l.contents
              .select { |o| o[:size] > 0 }
              .select{ |x| x.key.include?('csv/') }
              .map { |x| "#{base}#{x.key}" }

      dict = {
        fileLocations: [{
          URIs: urls
        }],
        globalUploadSettings: { format: 'CSV', delimiter: '|' }
      }

      nice_json = JSON.pretty_generate(dict.as_json)

      File.open(file, 'w') do |f|
        f.write nice_json
      end

      logger.info 'Putting to S3...'
      object = S3Client.put_object(
        body: nice_json    ,
        bucket: 'dmi-bi-logs-test',
        key: 'qs-manifest-latest.json'
      )
end

    def s3_client
      client = Aws::S3::Client.new
    end

    def s3_bucket
      'dmi-bi-logs-test'
    end
  end
end
