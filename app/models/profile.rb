require 'active_record/diff'

class Profile < ApplicationRecord
  include ActiveRecord::Diff
  diff :platform_id, :card_holder_name, :email,
       :photo, :photo_upload_allowed, :card_number,
       :valid_from, :valid_to, :is_profile_present_on_ccdb

  has_one :sibling, class_name: "Profile"
  after_destroy :destroy_sibling
  belongs_to :report

  attribute :differences

  def destroy_sibling
    sibling&.destroy
  end

  def sibling_difference
    diff sibling
  end

  def differences
    sibling_difference.keys
  end


  def self.import_json_file(file_name = nil, destroy_old = true)
    json = read_json file_name

    import_json(json, destroy_old)
  end

  def self.import_json(json = nil, destroy_old = true)
    Profile.transaction do
      if destroy_old
        report = Report.where(version: json['version']).first
        report&.destroy!
      end
      report = Report.find_or_create_by!(
          version: json['version']
      )

      report.update(
          date: Time.current,
          count: json['total'],
      )

      differences = json['differences']
      logger.info "Found #{differences.count} items"

      differences.each.with_index(1) do |d, i|
        mw = d.dig('mw')
        ccdb = d.dig('ccdb')

        old_profile = nil
        [mw, ccdb].each_with_index do |p, pi|
          next unless p.present?
          profile = Profile.create!(
              platform_id: p.dig('profileId'),
              card_holder_name: p.dig('cardHolderName'),
              email: p.dig('email'),
              photo: p.dig('photo'),
              photo_upload_allowed: p.dig('photoUploadAllowed'),
              card_number: p.dig('cardNumber'),
              valid_to: Time.at(p.dig('validTo').to_i/1000),
              valid_from: Time.at(p.dig('validFrom').to_i/1000),
              is_profile_present_on_ccdb: d.dig('isProfilePresentOnCcdb'),
              platform_name: pi.zero? ? "mw" : "ccdb",
              report: report,
          )

          if pi == 1
            profile.update! sibling: old_profile
            old_profile.update! sibling: profile
          end
          old_profile = profile

        end
        logger.info "Processed #{(i.to_f/differences.count*100).to_s[0..5]}% - #{i} of #{differences.count}" if i%100 == 0

      end
    end

    nil
  end


  private


  def self.destroy_all_profiles!
    logger.warn "Destroying all #{Profile.all.count} profiles in DB"
    Profile.transaction do
      Profile.delete_all
    end
    logger.warn "Done"
  end

  def self.read_json(file_name)
    f_contents = File.read(file_name)
    JSON.parse(f_contents)
  end


end
