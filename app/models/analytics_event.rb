class AnalyticsEvent < ApplicationRecord

  def fire!(opts = {})
    logger.tagged "Firing #{id}" do
      platforms = opts[:only] || [:belladati, :firehose, :kinesis]

      AnalyticsEvent.transaction do
        fire_to_belladati if platforms.include? :belladati
        fire_to_kinesis_stream if platforms.include? :kinesis

        self.update! fired: true
      end
    end
  end

  def fire_to_firehose(opts={})
    FirehoseClient.put_record(
          delivery_stream_name: opts[:delivery_stream_name] || 'mw-stream-s3',
          record: {
            data: "#{self.to_json}\n"
          }
        )

    logger.debug "Streamed to Firehose #{self.id} event"
  end

  def fire_to_kinesis_stream
    client = Aws::Kinesis::Client.new
    client.put_record partition_key: self.id.to_s, stream_name: 'mw-stream', data: "#{self.to_json}\n"

    logger.debug "Streamed to Kinesis #{self.id} event"
  end

  def to_belladati_json(opts = {})
    attrs = public_attributes
    attrs.slice!(*opts[:only]) if opts[:only].present?

    attrs = attrs.transform_keys do |k|
      if k != 'id'
       "L_#{k.upcase}"
     else
       k
     end
    end

    attrs.transform_values! &:to_s

  end

  def from_log_line(log_line = '')
    Time.use_zone('UTC') do
      ll = log_line.split('|').map { |x| x == "null" ? nil : x }
      latlon = ll[1]&.gsub('latitude=', '')&.gsub('longitude=', '')&.gsub('LatLon', '')&.gsub(' ', '')&.gsub('(', '')&.gsub(')', '')

      self.assign_attributes(
        datetime: Time.zone.strptime(ll[0], '%H:%M:%S_%d%m%Y'),
        latitude: latlon&.split(',')&.first,
        longitude: latlon&.split(',')&.second,
        user_agent: ll[2]&.strip,
        ip: ll[3]&.strip,
        country_id: ll[4]&.strip,
        institute_id: ll[5]&.strip,
        language: ll[6]&.strip,
        app_version: ll[7]&.strip,
        user_id: ll[8]&.strip,
        card_id: ll[9]&.strip,
        ccdb_card_id: ll[10]&.strip,
        session_id: ll[11]&.strip,
        action_type_code: ll[12]&.strip,
        action_params: ll[13]&.strip,
      )
    end

    self
  end

  # Sample
  # 00:02:55_11012018|LatLon(latitude=-45.03023, longitude=168.66271)|iOS|125.239.128.28|null|null|en|3.11|103195|103422|24162055|1697580|0302|
  # 10:55:46_07082017|48.85341,2.3488|Android|82.12.182.192|2635167|67|en|3.6|22180|22088|21783491|987480|0302|
  class << self

    def import_log_file(fname, each = 2)
      events = []
      @@lines ||= File.readlines(fname)
      @@lines.each_with_index do |line, idx|
        events << AnalyticsEvent.new.from_log_line(line)

        if idx % each == 0
          self.import events, validate: false
          logger.info "Processed #{idx} events..."
          events = []
        end
      end

      self.import events, validate: false
    end

    def stream_events(opts ={})
      eps = opts[:eps] || 1
      idx = 1
      logger.tagged "Stream" do

        count = AnalyticsEvent.where.not(latitude: nil).where.not(fired: true).count
        events = AnalyticsEvent
          .where.not(latitude: nil)
          .where.not(fired: true)
          .offset((SecureRandom.random_number(0..0.7)*count).to_i)
          .order(datetime: :asc)
        logger.info "Found #{count} events to stream"

        events.each do |e|
          Thread.new do
            logger.debug "Streaming idx #{idx += 1}"
            e.fire! only: [:kinesis]
          end
          delay = 1.0/eps
          jitter = SecureRandom.random_number(0.7..1.3)
          sleep delay * jitter
        end

        logger.info "Dispatched all events"
      end
    end


    def fire_list!(events = [], opts = {})
      cols = ['ccdb_card_id', 'ip', 'language', 'session_id', 'user_id']

      events.map! { |e| e.to_belladati_json(only: cols) }

      dict = {
        columns: cols.map! { |c| { code: "L_#{c.upcase}" } },
        data: events.map(&:values),
      }
      dict = dict.to_json

      token = AnalyticsEvent.new.belladati_auth_token

      uri = URI.parse("https://service.belladati.com/api/import/33808?oauth_consumer_key=#{Rails.application.secrets[:belladati_oauth_consumer_key]}&oauth_token=#{token}&oauth_timestamp=#{Time.now.to_i}&oauth_nonce=#{SecureRandom.uuid}")
      request = Net::HTTP::Post.new(uri)
      request.content_type = "application/octet-stream"
      request.body = dict

      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end
      "BD Response: #{response.body}"
    end

    def import_log_line(log_line = '')
      AnalyticsEvent.new.from_log_line(log_line).save!
    end
  end


  def public_attributes
    attrs = attributes.except 'created_at', 'updated_at', 'fired', 'uuid', 'id'
    attrs['location'] = "#{latitude},#{longitude}"
    attrs
  end


  def fire_to_belladati
    logger.info "Firing #{self.id} to belladati..."
    logger.debug "Streaming #{self.to_belladati_json}..."
    uri = URI.parse("https://service.belladati.com/api/dataSets/33808/data")
    request = Net::HTTP::Post.new(uri)
    request.content_type = "application/x-www-form-urlencoded"
    request.set_form_data(
      "oauth_consumer_key" => Rails.application.secrets[:belladati_oauth_consumer_key],
      "oauth_nonce" => "#{SecureRandom.uuid}",
      "oauth_timestamp" => "#{Time.now.to_i}",
      "oauth_token" => belladati_auth_token,
      "dataRow" => self.to_belladati_json.to_json,
    )

    req_options = {
      use_ssl: uri.scheme == "https",
    }

    response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
      http.request(request)
    end

    logger.info "Fired #{self.id} to Belladati. Response #{response.code} #{response.body}"
  end

  def belladati_auth_token
    token = Rails.cache.fetch('belladati-auth-token', expires_in: 5.minutes) do
      logger.warn "Requesting auth token from Belladati..."
      require 'net/http'
      require 'uri'

      uri = URI.parse("https://service.belladati.com/oauth/accessToken")
      request = Net::HTTP::Post.new(uri)
      request.content_type = "application/x-www-form-urlencoded"
      request.set_form_data(
        "oauth_consumer_key" => Rails.application.secrets[:belladati_oauth_consumer_key],
        "oauth_nonce" => "#{SecureRandom.uuid}",
        "oauth_timestamp" => "#{Time.now.to_i}",
        "x_auth_username" => Rails.application.secrets[:belladati_xauth_username],
        "x_auth_password" => Rails.application.secrets[:belladati_xauth_password],
      )

      req_options = {
        use_ssl: uri.scheme == "https",
      }

      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
      end

      response.body.split('&').first.split('=').last
    end
    logger.info "Auth Token: #{token}"

    token
  end

  private

end
