class ReportsController < ApplicationController
  http_basic_authenticate_with name: Rails.application.secrets.basic_auth_username, password: Rails.application.secrets.basic_auth_password
  before_action :set_report, only: [:show, :destroy, :summary]

  def index
    @reports = Report.all.order(version: :desc)
  end

  def show
  end

  def destroy
    @report.destroy
  end

  def summary
    key = @report

    @summary = Rails.cache.fetch(key, expires_in: 30.seconds) do
      logger.info "Cache miss. Retrieving report..."
      @report.diff_report
    end
  end

  def import
    begin
      text = params[:import_file].read
      json = JSON.parse(text)
      Profile.import_json(json, true)
      report = Report.last
      flash[:success] = "Report imported. Version: #{report.version}. Profiles: #{report.profiles.count/2}"

    rescue StandardError => error
      flash[:error] = "There was an error importing: #{error.inspect}"
    ensure
      redirect_to reports_path
    end

  end

  private

  def set_report
    @report = Report.find(params[:id])
  end
end
