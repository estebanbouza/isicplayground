class ProfilesController < ApplicationController
  http_basic_authenticate_with name: Rails.application.secrets.basic_auth_username, password: Rails.application.secrets.basic_auth_password
  before_action :set_profile, only: [:show, :edit, :update, :destroy]

  # GET /profiles
  # GET /profiles.json
  def index
    @report = Report.find(params[:report_id])
    @q = @report.profiles.ransack(params[:q])
    @profiles = @q.result(distinct: true).includes(:sibling).page(params[:page])
  end


  # GET /profiles/1
  # GET /profiles/1.json
  def show
  end

  # GET /profiles/new
  def new
    @profile = Profile.new
  end

  # GET /profiles/1/edit
  def edit
  end

  # POST /profiles
  # POST /profiles.json
  def create
    @profile = Profile.new(profile_params)

    respond_to do |format|
      if @profile.save
        format.html {redirect_to @profile, notice: 'Profile was successfully created.'}
        format.json {render :show, status: :created, location: @profile}
      else
        format.html {render :new}
        format.json {render json: @profile.errors, status: :unprocessable_entity}
      end
    end
  end

  # PATCH/PUT /profiles/1
  # PATCH/PUT /profiles/1.json
  def update
    respond_to do |format|
      if @profile.update(profile_params)
        format.html {redirect_to @profile, notice: 'Profile was successfully updated.'}
        format.json {render :show, status: :ok, location: @profile}
      else
        format.html {render :edit}
        format.json {render json: @profile.errors, status: :unprocessable_entity}
      end
    end
  end

  # DELETE /profiles/1
  # DELETE /profiles/1.json
  def destroy
    @profile.destroy
    respond_to do |format|
      format.html {redirect_to profiles_url, notice: 'Profile was successfully destroyed.'}
      format.json {head :no_content}
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_profile
    @profile = Profile.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def profile_params
    params.require(:profile).permit(:platform_id, :card_holder_name, :email, :photo, :photo_upload_allowed, :card_number, :valid_from, :valid_to, :is_profile_present_on_ccdb)
  end
end
