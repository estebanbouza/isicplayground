class AnalyticsEventsController < ApplicationController
  before_action :set_analytics_event, only: [:show, :edit, :update, :fire, :destroy]
  around_action :wrap_transaction, only: [:import_analytics_logfile]

  # GET /analytics_events
  # GET /analytics_events.json
  def index
    @q = AnalyticsEvent.ransack params[:q]
    @analytics_events = @q.result.page params[:page]
  end

  # GET /analytics_events/1
  # GET /analytics_events/1.json
  def show
  end

  # GET /analytics_events/new
  def new
    @analytics_event = AnalyticsEvent.new
  end

  # GET /analytics_events/1/edit
  def edit
  end

  def fire
    @analytics_event.fire!
    flash[:success] = "Event id #{@analytics_event.id} from #{@analytics_event.datetime} fired"
    redirect_to analytics_events_path

  end


  def import_analytics_logfile
    logger.info "Importing analytics logfile"
    begin
      idx = 0
      File.readlines(params[:import_file].tempfile).each do |line|
        AnalyticsEvent.import_log_line line
        logger.info "Imported #{idx} events" if idx % 100 == 0
      end

      flash[:success] = "Events imported."

    rescue StandardError => error
      flash[:error] = "There was an error importing: #{error.message}"
      raise error
    ensure
      redirect_to analytics_events_path
    end
  end

  # POST /analytics_events
  # POST /analytics_events.json
  def create
    @analytics_event = AnalyticsEvent.new(analytics_event_params)

    respond_to do |format|
      if @analytics_event.save
        format.html { redirect_to @analytics_event, notice: 'Analytics event was successfully created.' }
        format.json { render :show, status: :created, location: @analytics_event }
      else
        format.html { render :new }
        format.json { render json: @analytics_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /analytics_events/1
  # PATCH/PUT /analytics_events/1.json
  def update
    respond_to do |format|
      if @analytics_event.update(analytics_event_params)
        format.html { redirect_to @analytics_event, notice: 'Analytics event was successfully updated.' }
        format.json { render :show, status: :ok, location: @analytics_event }
      else
        format.html { render :edit }
        format.json { render json: @analytics_event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /analytics_events/1
  # DELETE /analytics_events/1.json
  def destroy
    @analytics_event.destroy
    respond_to do |format|
      format.html { redirect_to analytics_events_url, notice: 'Analytics event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_analytics_event
      @analytics_event = AnalyticsEvent.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def analytics_event_params
      params.fetch(:analytics_event, {})
    end

    def wrap_transaction
      AnalyticsEvent.transaction { yield }
    end
end
