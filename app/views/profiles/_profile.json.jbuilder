json.extract! profile, :id, :platform_id, :card_holder_name, :email, :photo, :photo_upload_allowed, :card_number, :valid_from, :valid_to, :is_profile_present_on_ccdb, :created_at, :updated_at
json.url profile_url(profile, format: :json)
