json.extract! analytics_event, :id, :created_at, :updated_at
json.url analytics_event_url(analytics_event, format: :json)
