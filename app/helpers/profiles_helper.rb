module ProfilesHelper
  def field_color_for(field)
    colors = [:blue, :red, :olive, :brown, :violet, :yellow, :pink, :purple, :orange, :green]
    fields = [:photo, :card_number, :valid_from, :valid_to, :photo_upload_allowed, :card_holder_name, :email]

    colors[fields.index(field)%colors.count].to_s
  end
end
