require 'test_helper'

class ProfilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @profile = profiles(:one)
  end

  test "should get index" do
    get profiles_url
    assert_response :success
  end

  test "should get new" do
    get new_profile_url
    assert_response :success
  end

  test "should create profile" do
    assert_difference('Profile.count') do
      post profiles_url, params: { profile: { card_holder_name: @profile.card_holder_name, card_number: @profile.card_number, email: @profile.email, is_profile_present_on_ccdb: @profile.is_profile_present_on_ccdb, photo: @profile.photo, photo_upload_allowed: @profile.photo_upload_allowed, platform_id: @profile.platform_id, valid_from: @profile.valid_from, valid_to: @profile.valid_to } }
    end

    assert_redirected_to profile_url(Profile.last)
  end

  test "should show profile" do
    get profile_url(@profile)
    assert_response :success
  end

  test "should get edit" do
    get edit_profile_url(@profile)
    assert_response :success
  end

  test "should update profile" do
    patch profile_url(@profile), params: { profile: { card_holder_name: @profile.card_holder_name, card_number: @profile.card_number, email: @profile.email, is_profile_present_on_ccdb: @profile.is_profile_present_on_ccdb, photo: @profile.photo, photo_upload_allowed: @profile.photo_upload_allowed, platform_id: @profile.platform_id, valid_from: @profile.valid_from, valid_to: @profile.valid_to } }
    assert_redirected_to profile_url(@profile)
  end

  test "should destroy profile" do
    assert_difference('Profile.count', -1) do
      delete profile_url(@profile)
    end

    assert_redirected_to profiles_url
  end
end
