require 'test_helper'

class AnalyticsEventsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @analytics_event = analytics_events(:one)
  end

  test "should get index" do
    get analytics_events_url
    assert_response :success
  end

  test "should get new" do
    get new_analytics_event_url
    assert_response :success
  end

  test "should create analytics_event" do
    assert_difference('AnalyticsEvent.count') do
      post analytics_events_url, params: { analytics_event: {  } }
    end

    assert_redirected_to analytics_event_url(AnalyticsEvent.last)
  end

  test "should show analytics_event" do
    get analytics_event_url(@analytics_event)
    assert_response :success
  end

  test "should get edit" do
    get edit_analytics_event_url(@analytics_event)
    assert_response :success
  end

  test "should update analytics_event" do
    patch analytics_event_url(@analytics_event), params: { analytics_event: {  } }
    assert_redirected_to analytics_event_url(@analytics_event)
  end

  test "should destroy analytics_event" do
    assert_difference('AnalyticsEvent.count', -1) do
      delete analytics_event_url(@analytics_event)
    end

    assert_redirected_to analytics_events_url
  end
end
