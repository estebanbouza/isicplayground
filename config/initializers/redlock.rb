servers = YAML.load(ERB.new(File.read('config/redis.yml')).result)[Rails.env]['url']

LockManager = Redlock::Client.new([servers])
