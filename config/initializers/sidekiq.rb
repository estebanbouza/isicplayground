Sidekiq::Extensions.enable_delay!

redis_conf = 'config/redis.yml'

Sidekiq.configure_server do |config|
  config.redis = { url: YAML.load(ERB.new(File.read(redis_conf)).result)[Rails.env]['url'] }
end

Sidekiq.configure_client do |config|
  config.redis = { url: YAML.load(ERB.new(File.read(redis_conf)).result)[Rails.env]['url'] }
end

schedule_conf = 'config/sidekiq-schedule.yml'
if File.exist?(schedule_conf) && Sidekiq.server?
  Sidekiq::Cron::Job.load_from_hash YAML.load_file(schedule_conf)
end

Sidekiq::Logging.logger.level = Logger::INFO
