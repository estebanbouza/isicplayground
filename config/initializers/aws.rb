Aws.config.update(
  region: 'us-west-2',
  credentials: Aws::Credentials.new(
    Rails.application.secrets[:aws_access_key_id],
    Rails.application.secrets[:aws_secret_access_key]
  )
)

::KinesisClient = Aws::Kinesis::Client.new
::FirehoseClient = Aws::Firehose::Client.new
::S3Client = Aws::S3::Client.new
::EsClient = Aws::ElasticsearchService::Client.new
