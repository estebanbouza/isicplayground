require 'sidekiq/web'
require 'sidekiq/cron/web'

Rails.application.routes.draw do
  root "pages#home"
  get 'pages/home'

  mount Sidekiq::Web => '/jobs'

  get 'reports/index'

  get 'reports/show'

  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  root "reports#index"

  get "/profiles/report", "profiles#report"

  resources :reports, only: [:index, :show, :destroy] do
    resources :profiles, only: [:index, :show]
    get 'summary', on: :member
  end

  resources :analytics_events do
    post 'fire', on: :member
  end


  post 'import-report', to: 'reports#import'
  post 'import-analytics-logfile', to: 'analytics_events#import_analytics_logfile'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
